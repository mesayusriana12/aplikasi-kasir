# Aplikasi Kasir (Kasirku)

## Fitur

* Perhitungan transaksi secara live :moneybag:
* Filter data dengan banyak opsi :white_check_mark:
* Print dan Download PDF Invoice Transaksi :page_facing_up:
* Tampilan sederhana dan mudah dimengerti :computer:

## Library

* [Laravel Sweetalert](https://github.com/realrashid/sweet-alert)
* [Laravel Livewire](https://github.com/livewire/livewire)
* [Sweetalert2](https://github.com/sweetalert2/sweetalert2)
* [Select2](https://github.com/select2/select2)
* [Bootstrap Datepicker](https://github.com/uxsolutions/bootstrap-datepicker)

## Instalasi

1. Dengan terminal Gitbash atau sejenisnya lakukan Git Clone dari repository ini.

 ```
 git clone https://gitlab.com/mesayusriana12/aplikasi-kasir.git
 ```
2. Masuk ke dalam folder 'aplikasi-kasir'

```
cd aplikasi-kasir
```
3. Install package yang diperlukan dari composer.

```
 composer install
 ```

 jika gagal, gunakan

 ```
 composer update
 ```

4. Copy file .env.example ke file .env (buat baru), kemudian masukan nama database dan akun untuk database.

file .env
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=aplikasi-kasir
DB_USERNAME=root
DB_PASSWORD=
```

5. Lakukan migrasi dengan seed yang telah disediakan.

 ```
 php artisan migrate:fresh --seed
 ```

6. Jalankan aplikasi secara lokal.

 ```
 php artisan serve
 ```
7. Login dengan menggunakan akun yang telah disediakan pada seeder.

| username | email | password | role |
| -------- | ----- | ---------| ---- |
| admin | admin@kasirku.id | admin | Admin Kasir |
| kasir | kasir@kasirku.id | kasir | Kasir Biasa |
