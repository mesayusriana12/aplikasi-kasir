<?php

namespace App\Http\Livewire;

use App\Models\MasterBarang;
use Livewire\Component;

class TransaksiPembelian extends Component
{
    public $cart = [];
    public $get_barang = [];
    public $total = 0;
    protected $listeners = [
        'selectedBarang'
    ];

    public function mount()
    {
        $this->get_barang = MasterBarang::all();
        $this->cart = [];
    }

    public function tambahBarang()
    {
        $this->cart[] = ['barang_id' => '', 'jumlah' => 1];
        $this->dispatchBrowserEvent('reApplySelect2');
    }

    public function hapusBarang($index)
    {
        unset($this->cart[$index]);
        $this->cart = array_values($this->cart);
        $this->calculate($this->cart);
    }

    public function increment($index)
    {
        $this->cart[$index]['jumlah']++;
        $this->calculate($this->cart);
    }
    
    public function decrement($index)
    {
        $this->cart[$index]['jumlah'] == 0 ? 0 : $this->cart[$index]['jumlah']--;
        $this->calculate($this->cart);
    }

    public function selectedBarang($index, $value)
    {
        $this->cart[$index]['barang_id'] = $value;
        $find = MasterBarang::find($value);
        $this->cart[$index]['harga_satuan'] = $find->harga_satuan;
        $this->calculate($this->cart);
        
        $chosen = [];
        foreach ($this->cart as $item) {
            $item['barang_id'] != '' ? array_push($chosen, $item['barang_id']) : false;
        }
        $this->get_barang = MasterBarang::whereNotIn('id', $chosen)->get();
    }

    public function calculate($cart)
    {
        $this->total = 0;
        foreach ($this->cart as &$item) {
            if($item['barang_id'] != ''){
                $item['subtotal'] = $item['harga_satuan'] * $item['jumlah'];
                $this->total += $item['subtotal'];
            }
        }
        unset($item);
    }
    public function render()
    {
        return view('livewire.transaksi-pembelian', [
            'cart' => $this->cart,
            'total' => $this->total
        ]);
    }
}
