<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PembelianController extends Controller
{
    public function start()
    {
        return view('pembelian.start');
    }

    public function bought(Request $request)
    {
        DB::beginTransaction();
        try {
            TransaksiPembelian::create([
                'total_harga' => $request->total,
                'user_id' => Auth::user()->id
            ]);
            $last_id = TransaksiPembelian::latest()->first()->id;

            foreach ($request->cart as $item) {
                $find = MasterBarang::find($item['barang_id']);
                $subtotal = $find->harga_satuan * $item['jumlah'];
                TransaksiPembelianBarang::create([
                    'transaksi_pembelian_id' => $last_id,
                    'master_barang_id' => $item['barang_id'],
                    'jumlah' => $item['jumlah'],
                    'harga_satuan' => $find->harga_satuan,
                    'subtotal' => $subtotal
                ]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        
        Alert::success('Berhasil!', 'Transaksi berhasil dilakukan!');
        return redirect(route('pembelian.' . ($request->print ? 'print' : 'detail'), [$last_id]));
        
    }

    public function history()
    {
        $filtered_barang = [];
        if (request('barang')) {
            foreach (request('barang') as $id) {
                $find = MasterBarang::find($id)->nama_barang;
                array_push($filtered_barang, $find);
            }
        }
        $filtered_user = [];
        if (request('user')) {
            foreach (request('user') as $id) {
                $find = User::find($id)->name;
                array_push($filtered_user, $find);
            }
        }
        return view('pembelian.history',[
            'data' => TransaksiPembelian::role()->search()->paginate(20),
            'barang' => MasterBarang::all(),
            'user' => User::all(),
            'filtered_barang' => $filtered_barang,
            'filtered_user' => $filtered_user
        ]);
    }

    public function detail($id)
    {
        $data = TransaksiPembelian::role()->find($id);
        if ($data === NULL) {
            Alert::error('Unauthorized', 'Anda tidak diizinkan untuk mengakses halaman ini!');
            return redirect()->back();
        }
        return view('pembelian.detail',[
            'data_pembelian' => $data,
            'data_barang' => TransaksiPembelianBarang::where('transaksi_pembelian_id', $id)->get()
        ]);
    }

    public function print($id)
    {
        $data = TransaksiPembelian::role()->find($id);
        if ($data === NULL) {
            Alert::error('Unauthorized', 'Anda tidak diizinkan untuk mengakses halaman ini!');
            return redirect()->back();
        }
        return view('pembelian.print',[
            'data_pembelian' => $data,
            'data_barang' => TransaksiPembelianBarang::where('transaksi_pembelian_id', $id)->get()
        ]);
    }
}
