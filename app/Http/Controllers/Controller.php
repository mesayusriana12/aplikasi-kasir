<?php

namespace App\Http\Controllers;

use DateTime;
use DatePeriod;
use DateInterval;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function dashboard()
    {
        $daysInWeek = $this->daysInWeek(date('W'));
        if (Auth::user()->role_id != 1) {
            $countTransactionToday = TransaksiPembelian::whereDate('created_at', date('Y-m-d'))->where('user_id', Auth::user()->id)->count();

            return view('dashboard.dashboard', [
                'transactionToday' => $countTransactionToday ?? 0
            ]);
        }
        
        $countTransactionToday = TransaksiPembelian::whereDate('created_at', date('Y-m-d'))->count();
        $incomeToday = TransaksiPembelian::whereDate('created_at', date('Y-m-d'))->sum('total_harga');
        $incomeThisWeek = TransaksiPembelian::whereBetween('created_at', [$daysInWeek[0], $daysInWeek[6]])->sum('total_harga');
        
        $top5ProductThisWeek = TransaksiPembelianBarang::join('master_barang', 'master_barang_id', '=', 'master_barang.id')
            ->whereBetween('transaksi_pembelian_barang.created_at', [$daysInWeek[0], $daysInWeek[6]])
            ->selectRaw('SUM(jumlah) AS terjual, nama_barang')
            ->groupBy('master_barang_id')
            ->orderBy('terjual', 'desc')
            ->limit(5)
            ->get();

        $top5TransactionThisWeek = TransaksiPembelian::whereBetween('created_at', [$daysInWeek[0], $daysInWeek[6]])
            ->select('id','total_harga','created_at AS tanggal')
            ->orderBy('total_harga', 'desc')
            ->limit(5)
            ->get();
        
        return view('dashboard.dashboard', [
            'transactionToday' => $countTransactionToday ?? 0,
            'incomeToday' => $incomeToday ?? 0,
            'incomeThisWeek' => $incomeThisWeek ?? 0,
            'top5ProductThisWeek' => $top5ProductThisWeek ?? [],
            'top5TransactionThisWeek' => $top5TransactionThisWeek ?? []
        ]);
    }

    static function daysInWeek($weekNum)
    {
        $result = array();
        $datetime = new DateTime('00:00:00');
        $datetime->setISODate((int)$datetime->format('o'), $weekNum, 1);
        $interval = new DateInterval('P1D');
        $week = new DatePeriod($datetime, $interval, 6);

        foreach($week as $day){
            $result[] = $day->format('Y-m-d');
        }
        return $result;
    }
}
