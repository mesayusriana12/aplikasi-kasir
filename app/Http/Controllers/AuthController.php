<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use RealRashid\SweetAlert\Facades\Alert;

class AuthController extends Controller
{
    public function login()
    {
        if (Auth::check()) {
            return redirect('/dashboard');
        } else {
            return view('auth.login');
        }
    }

    public function authenticate(Request $request)
    {
        Cookie::forget('XSRF-TOKEN');
        Cookie::forget('aplikasi_kasir_session');
        
        $request->session()->flush();
        
        $check = [
            'username'  => $request->input('username'),
            'password'  => $request->input('password'),
        ];
        
        Auth::attempt($check);
        
        if (Auth::check()) { 
            Auth::logoutOtherDevices($request->password);
            Alert::toast('Selamat datang "' . Auth::user()->name . '" di aplikasi Kasirku!','success')->width('500px');
            return redirect('/dashboard');
        } else { 
            $check = [
                'email'  => $request->input('username'),
                'password'  => $request->input('password'),
            ];
            unset($check['username']);
            
            Auth::attempt($check);
            
            if (Auth::check()) { 
                Auth::logoutOtherDevices($request->password);
                Alert::toast('Selamat datang "' . Auth::user()->name . '" di aplikasi Kasirku!','success')->width('500px');
                return redirect('/dashboard');
            } else {
                Alert::error('Login gagal!', 'Username atau Password salah!');
                return redirect('/login');
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        Alert::toast('Anda telah logout dari aplikasi Kasirku!','info')->width('500px');
        return redirect('/login');
    }
}
