<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use App\Models\TransaksiPembelianBarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('barang.index', [
            'data' => MasterBarang::search()->paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_barang' => 'required',
            'harga_satuan' => 'required'
        ]);
        
        DB::beginTransaction();
        try {
            MasterBarang::create($validated);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        
        Alert::success('Berhasil!', 'Data barang berhasil dibuat!');
        return redirect(route('barang.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('barang.edit', [
            'data' => MasterBarang::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nama_barang' => 'required',
            'harga_satuan' => 'required'
        ]);
        
        DB::beginTransaction();
        try {
            MasterBarang::where('id', $id)->update($validated);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        
        Alert::success('Berhasil!', 'Data barang berhasil diupdate!');
        return redirect(route('barang.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = TransaksiPembelianBarang::where('master_barang_id', $id)->first();
        if ($check !== NULL) {
            Alert::error('Gagal!', 'Data barang tidak dapat dihapus karena telah tercatat dalam transaksi!')->width(700);
            return redirect(route('barang.index'));
        }
        DB::beginTransaction();
        try {
            MasterBarang::destroy($id);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        Alert::success('Berhasil!', 'Data barang berhasil dihapus!');
        return redirect(route('barang.index'));
    }
}
