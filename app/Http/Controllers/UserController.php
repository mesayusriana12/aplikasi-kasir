<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\TransaksiPembelian;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->role_id == 1) {
            return view('user.index', [
                'data' => User::search()->paginate(20)
            ]);
        }
        else {
            Alert::error('Unauthorized!', 'Anda tidak diperkenankan untuk mengakses halaman in!');
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role_id == 1) {
            return view('user.create');
        }
        else {
            Alert::error('Unauthorized!', 'Anda tidak diperkenankan untuk mengakses halaman in!');
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->role_id == 1) {
            $validated = $request->validate([
                'name' => 'required|alpha_spaces',
                'username' => 'required|alpha_num|unique:users,username',
                'email' => 'required|email|unique:users,email',
                'role_id' => 'required'
            ]);
            $validated['password'] = Hash::make('kasirku');

            DB::beginTransaction();
            try {
                User::create($validated);
            } catch (\Exception $e) {
                DB::rollback();
                throw $e;
            }
            DB::commit();
            
            Alert::success('Berhasil!', 'Data kasir berhasil dibuat!');
            return redirect(route('user.index'));
        }
        else {
            Alert::error('Unauthorized!', 'Anda tidak diperkenankan untuk mengakses halaman in!');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->role_id == 1) {
            return view('user.edit', [
                'data' => User::find($id)
            ]);
        }
        else {
            Alert::error('Unauthorized!', 'Anda tidak diperkenankan untuk mengakses halaman in!');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->role_id == 1) {
            $validated = $request->validate([
                'name' => 'required|alpha_spaces',
                'username' => 'required|alpha_num|unique:users,username,'. $id . ',id',
                'email' => 'required|email|unique:users,email,'. $id . ',id',
                'role_id' => 'required',
            ]);

            DB::beginTransaction();
            try {
                User::where('id', $id)->update($validated);
            } catch (\Exception $e) {
                DB::rollback();
                throw $e;
            }
            DB::commit();
            
            Alert::success('Berhasil!', 'Data kasir berhasil diupdate!');
            return redirect(route('user.index'));
        }
        else {
            Alert::error('Unauthorized!', 'Anda tidak diperkenankan untuk mengakses halaman in!');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->role_id == 1) {
            $check = TransaksiPembelian::where('user_id', $id)->first();
            if ($check !== NULL) {
                Alert::error('Gagal!', 'Data kasir tidak dapat dihapus karena telah tercatat dalam transaksi!')->width(700);
                return redirect(route('user.index'));
            }
            DB::beginTransaction();
            try {
                User::destroy($id);
            } catch (\Exception $e) {
                DB::rollback();
                throw $e;
            }
            DB::commit();

            Alert::success('Berhasil!', 'Data kasir berhasil dihapus!');
            return redirect(route('user.index'));
        }
        else {
            Alert::error('Unauthorized!', 'Anda tidak diperkenankan untuk mengakses halaman in!');
            return redirect()->back();
        }
    }

    public function respass($id)
    {
        if(Auth::user()->role_id == 1) {
            DB::beginTransaction();
            try {
                User::where('id', $id)->update(['password' => Hash::make('kasirku')]);
            } catch (\Exception $e) {
                DB::rollback();
                throw $e;
            }
            DB::commit();
            
            Alert::success('Berhasil!', 'Password kasir terpilih berhasil direset!');
            return redirect(route('user.index'));
        }
        else {
            Alert::error('Unauthorized!', 'Anda tidak diperkenankan untuk mengakses halaman in!');
            return redirect()->back();
        }
    }

    public function profile()
    {
        return view('user.profile', [
            'data' => User::find(Auth::user()->id)
        ]);
    }

    public function profile_update(Request $request)
    {
        $id = Auth::user()->id;
        $validated = $request->validate([
            'name' => 'required|alpha_spaces',
            'username' => 'required|alpha_num|unique:users,username,'. $id . ',id',
            'email' => 'required|email|unique:users,email,'. $id . ',id',
        ]);

        if($request->password) {
            $validated['password'] = Hash::make($request->password);
        }

        DB::beginTransaction();
        try {
            User::where('id', $id)->update($validated);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
        
        Alert::success('Berhasil!', 'Profil berhasil diupdate!');
        return redirect(route('user.profile'));
    }
}
