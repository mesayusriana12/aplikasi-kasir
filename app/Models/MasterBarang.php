<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterBarang extends Model
{
    use HasFactory;
    protected $table = 'master_barang';
    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'nama_barang',
        'harga_satuan'
    ];

    public function pembelian_barang()
    {
        return $this->hasMany(TransaksiPembelianBarang::class, 'id');
    }
    
    public function scopeSearch($query)
    {
        if (request('nama'))
        {
            $query->where('nama_barang', 'like', '%' . request('nama') . '%');
        }
        if (request('harga'))
        {
            $query->where('harga_satuan', (request('op') == 'lt' ? '<' : (request('op') == 'gt' ? '>' : '=')), request('harga'));
        }
        return $query;
    }
}
