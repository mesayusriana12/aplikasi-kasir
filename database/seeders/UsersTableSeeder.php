<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin Kasir',
            'username' => 'admin',
            'email' => 'admin@kasirku.id',
            'password' => Hash::make('admin'),
            'role_id' => 1
        ]);
        User::create([
            'name' => 'Kasir',
            'username' => 'kasir',
            'email' => 'kasir@kasirku.id',
            'password' => Hash::make('kasir'),
            'role_id' => 2
        ]);
    }
}
