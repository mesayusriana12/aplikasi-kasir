<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Kasirku | Print Invoice</title>

    <link href="{{ asset('plugins/fontawesome/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet">

    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">

</head>

<body id="page-top" onload="window.print()">
    <div id="wrapper">
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <div class="container-fluid">
                    <div class="d-flex align-items-center mt-3">
                        <div>
                            <img src="{{ asset('img/logo.png') }}" alt="Logo Kasirku" width="96px">
                        </div>
                        <div class="ml-3">
                            <h1>Kasirku</h1>
                            <p>Jalan R.A.A Wiranata Kusumah No. 1, Baleendah Kab. Bandung</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row mb-3">
                        <div class="col-sm-6">
                            <div>Dilayani Oleh : <strong>{{ $data_pembelian->user->name }}</strong></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="text-right">Tanggal Transaksi : <strong>{{ tanggalTransaksi($data_pembelian->created_at) }}</strong></div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr role="row" style="background-color:#dadada">
                                    <th class="text-center" style="width: 5%;">No</th>
                                    <th>Nama Barang</th>
                                    <th class="text-center" style="width: 10%;">Jumlah</th>
                                    <th class="text-right">Harga Satuan</th>
                                    <th class="text-right">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data_barang as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->barang->nama_barang }}</td>
                                    <td class="text-center">{{ $item->jumlah }}</td>
                                    <td class="text-right">{{ formatRupiah($item->barang->harga_satuan, 0) }}</td>
                                    <td class="text-right">{{ formatRupiah($item->subtotal, 0) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot role="row" style="background-color:#dadada">
                                <th colspan="4" class="text-right" style="font-size: 22px;"> <span class="text-black"> Total </span></th>
                                <th class="text-right"  style="font-size: 22px;">{{ formatRupiah($data_pembelian->total_harga, 0) }}</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <script src="{{ asset('plugins/jquery-easing/jquery.easing.min.js') }}"></script>

    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
</body>
</html>