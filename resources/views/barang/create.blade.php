@extends('layouts.template')
@section('title', 'Tambah Data Barang')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Tambah Data Barang</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('barang.store') }}" method="post" class="form-horizontal" id="form-create">
                    @csrf
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Nama Barang :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="nama_barang" maxlength="50" placeholder="Nama Barang" autocomplete="off" required value="{{ old('nama_barang') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Harga Satuan :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="harga_satuan" id="harga_satuan" placeholder="Harga Satuan" autocomplete="off" required value="{{ old('harga_satuan') }}" onkeyup="formatRupiah(this.id)">
                        </div>
                    </div>
                    <div class="text-right mt-1">
                        <a href="{{ route('barang.index') }}" class="btn btn-secondary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-arrow-left"></i>
                            </span>
                            <span class="text">Kembali</span>
                        </a>
                        <button type="submit" class="btn btn-success btn-icon-split" id="btn-submit" form="form-create">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="card mt-2">
                <div class="card-body">
                    <h5>Terdapat kesalahan: </h5>
                    <div class="text-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@push('script')
    <script>
        $('#form-create').on('submit', function () {
            let raw = $('input[name=harga_satuan]').val();
            let formatted = raw.replace(/rp|[.]/gi, "");
            $('input[name=harga_satuan]').val(formatted);
        });
        function formatRupiah(id) {
            let angka = document.getElementById(id).value;
            let number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            document.getElementById(id).value = 'Rp' + rupiah;
        }
    </script>
@endpush