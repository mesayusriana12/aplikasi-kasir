@extends('layouts.template')
@section('title', 'Data Barang')
@push('style')
<!-- SweetAlert -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/css/sweetalert2.min.css')}}">
    <style>
        input[type=number]::-webkit-outer-spin-button,
        input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type=number] {
            -moz-appearance:textfield;
        }
    </style>
@endpush

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Data Barang</h4>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <a href="{{ route('barang.create') }}" class="btn btn-primary btn-icon-split mr-1"
                            data-toggle="tooltip" data-placement="top" title="Tambah Data Barang">
                            <span class="icon text-white-50">
                                <i class="fas fa-plus"></i> 
                            </span>
                            <span class="text">Tambah Data</span>
                        </a>
                        <span data-toggle="modal" data-target="#modalFilter">
                            <a class="btn btn-info btn-icon-split mr-1" 
                                data-toggle="tooltip" data-placement="top" title="Filter Data Barang">
                                <span class="icon text-white-50">
                                    <i class="fas fa-filter"></i> 
                                </span>
                                <span class="text">Filter</span>
                            </a>
                        </span>
                        @if (request('nama') || request('harga'))
                            <a href="{{ route('barang.index') }}" class="btn btn-danger btn-icon-split mr-1"
                                data-toggle="tooltip" data-placement="top" title="Bersihkan Filter">
                                <span class="icon text-white-50">
                                    <i class="fas fa-broom"></i> 
                                </span>
                                <span class="text">Bersihkan Filter</span>
                            </a>
                        @endif
                    </div>
                </div>
                @if (request('nama') || request('harga'))
                    <p class="text-secondary">
                        Filter saat ini : <br>
                        {!! request('nama') ? '&emsp;&emsp; Nama Barang : ' . request('nama') . '<br>' : '' !!}
                        {!! request('harga') ? '&emsp;&emsp; Harga Satuan : ' . (request('op') == 'lt' ? 'Kurang dari ' : (request('op') == 'gt' ? 'Lebih dari ' : 'Sama dengan ')) . formatRupiah(request('harga'), 0) . '<br>' : '' !!}
                    </p>
                @endif
                @if ($data->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="table-user">
                            <thead>
                                <tr role="row" style="background-color:#dadada">
                                    <th class="text-center" style="width: 5%;">No</th>
                                    <th>Nama Barang</th>
                                    <th class="text-right">Harga Satuan</th>
                                    <th class="text-center" style="width: 15%;"><i class="fas fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                <tr>
                                    <td>{{ (($data->currentPage() - 1) * $data->perPage()) + $loop->iteration }}</td>
                                    <td>{{ $item->nama_barang }}</td>
                                    <td class="text-right">{{ formatRupiah($item->harga_satuan, 0) }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('barang.edit', [$item->id]) }}"
                                            class="btn btn-sm btn-warning btn-action mx-1" data-container="table"
                                            data-toggle="tooltip" data-placement="top" title="Edit Data Barang">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <form action="{{ route('barang.destroy', [$item->id]) }}" method="POST"
                                            id="form-delete-{{$item->id}}" style="display: inline">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm"
                                                onclick="submit_delete({{$item->id}})" data-container="table"
                                                data-toggle="tooltip" data-placement="top" title="Hapus Data Barang">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="d-flex justify-content-center mt-3">
                        {!! $data->links() !!}
                    </div>
                @else
                    <div class="text-center">
                        <h3>Tidak ada data ditemukan.</h3>
                        <img src="{{ asset('img/data_empty.jpg') }}" width="427px" height="240px">
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalFilter" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Filter Data</h5>
                    <button class="close" type="button" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('barang.index') }}" class="form-horizontal" id="form-filter">
                        <div class="text-secondary small mb-2">Filter dapat diisi semua atau hanya salah satunya.</div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Perbandingan Harga :</label>
                            <div class="col-lg-8">
                                <select class="form-control" name="op">
                                    <option value="" disabled selected>--Pilih salah satu--</option>
                                    <option value="lt">Kurang dari</option>
                                    <option value="gt">Lebih dari</option>
                                    <option value="eq">Sama dengan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Harga Satuan :</label>
                            <div class="col-lg-8">
                                <input type="number" class="form-control" name="harga" placeholder="Harga Satuan" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Nama Barang :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="nama" maxlength="50" placeholder="Nama Barang" autocomplete="off">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <input class="btn btn-success" type="submit" value="Filter" form="form-filter">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<!-- Script Tambahan -->
    <script src="{{ asset('plugins/sweetalert2/js/sweetalert2.all.min.js') }}"></script>
    <script type="text/javascript">
        $('[data-toggle="tooltip"]').tooltip();
        $('input[name=harga]').on('keyup', function () {
            $('input[name=harga]').val() != '' ? $('select[name=op]').prop('required', true) : $('select[name=op]').prop('required', false)
        });
        $('select[name=op]').on('change', function () {
            $('input[name=harga]').prop('required', true);
        });
        $('#form-filter').on('submit', function () {
            $('input[name=nama]').val() == '' ? $('input[name=nama]').removeAttr('name') : false;
            $('input[name=harga]').val() == '' ? $('input[name=harga]').removeAttr('name') : false;
        });
        
        function submit_delete(user_id) {
            event.preventDefault();
            Swal.fire({
                width: 680,
                title: 'Anda yakin ingin menghapus data terpilih?',
                text: "Data yang sudah di hapus tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value == true) {
                    $(`#form-delete-${user_id}`).submit();
                } else {
                    return false;
                }
            });
        }
    </script>
@endpush
