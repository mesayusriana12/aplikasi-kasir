<div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr role="row" style="background-color:#dadada">
                    <th class="text-center" style="width: 5%;">No</th>
                    <th>Nama Barang</th>
                    <th class="text-center" style="width: 17%;">Jumlah</th>
                    <th class="text-right" style="width: 5%;">Aksi</th>
                    <th class="text-right" style="width: 15%;">Subtotal</th>
                </tr>
            </thead>
            <tbody>
                <form action="{{ route('pembelian.bought') }}" class="form-horizontal" id="form-create" method="POST">
                    @csrf
                    @foreach ($cart as $item => $perItem)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>
                            <div wire:ignore>
                                <select name="cart[{{ $item }}][barang_id]" wire:model="cart.{{ $item }}.barang_id" 
                                class="form-control select-search" data-iteration="{{ $item }}" form="form-create">
                                    <option value="" selected disabled>--Pilih salah satu barang--</option>
                                    @foreach ($get_barang as $barang)
                                    <option value="{{ $barang->id }}"> {{ $barang->nama_barang }}
                                        ({{ formatRupiah($barang->harga_satuan, 0) }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="input-group input-group-joined">
                                <span class="input-group-text btn" wire:click.prevent="decrement({{ $item }})">
                                    <i class="fas fa-minus"></i>
                                </span>
                                <input class="form-control" type="number" id="jumlah-{{ $item }}" form="form-create"
                                    name="cart[{{ $item }}][jumlah]" min="1" wire:model="cart.{{ $item }}.jumlah" readonly>
                                <span class="input-group-text btn" wire:click.prevent="increment({{ $item }})">
                                    <i class="fas fa-plus"></i>
                                </span>
                            </div>
                        </td>
                        <td>
                            <button wire:click.prevent="hapusBarang({{ $item }})" class="btn btn-danger btn-sm"
                                title="Hapus Barang">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </td>
                        <td class="text-right">{{ formatRupiah($perItem['subtotal'] ?? 0, 0) }}</td>
                    </tr>
                    @endforeach
                </form>
            </tbody>
            @if ($total > 0)
                <input type="hidden" name="print" value="true" form="form-create" disabled>
                <input type="hidden" name="total" value="{{ $total }}" form="form-create">
                <tfoot role="row" style="background-color:#dadada">
                    <th colspan="4" class="text-right" style="font-size: 22px;"> <span class="text-black"> Total </span></th>
                    <th class="text-right"  style="font-size: 22px;">{{ formatRupiah($total, 0) }}</th>
                </tfoot>
            @endif
        </table>
    </div>
    <button wire:click.prevent="tambahBarang" class="btn btn-primary btn-icon-split mr-1" title="Tambah Barang">
        <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Barang</span>
    </button>
</div>

@push('script')
    <script>
        $(document).ready(function () {
            window.addEventListener('reApplySelect2', event => {
                $('.select-search').select2({
                    language: 'id',
                    width: '100%'
                });
                $('.select-search').on('change', function (e) {
                    let index = e.target.attributes["data-iteration"].value;
                    livewire.emit('selectedBarang', index, e.target.value)
                });
            });
        });
    </script>
@endpush
