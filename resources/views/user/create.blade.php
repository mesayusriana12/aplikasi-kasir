@extends('layouts.template')
@section('title', 'Tambah Data Kasir')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Tambah Data Kasir</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('user.store') }}" method="post" class="form-horizontal" id="form-create">
                    @csrf
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Nama Kasir :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="name" maxlength="100" placeholder="Nama Kasir" autocomplete="off" required value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Username :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="username" maxlength="50" placeholder="Username" autocomplete="off" required value="{{ old('username') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Email :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="email" maxlength="50" placeholder="Email" autocomplete="off" required value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Role :</label>
                        <div class="col-lg-4">
                            <select class="form-control" name="role_id">
                                <option value="" disabled selected>--Pilih salah satu--</option>
                                <option value="1">Admin</option>
                                <option value="2">Kasir</option>
                            </select>
                        </div>
                    </div>
                    <small class="text-secondary">Password user kasir akan dibuatkan secara default yaitu "kasirku", beritahu user untuk mengubah password setelah akun terbuat pada menu profil!</small>
                    <div class="text-right mt-1">
                        <a href="{{ route('user.index') }}" class="btn btn-secondary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-arrow-left"></i>
                            </span>
                            <span class="text">Kembali</span>
                        </a>
                        <button type="submit" class="btn btn-success btn-icon-split" id="btn-submit" form="form-create">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="card mt-2">
                <div class="card-body">
                    <h5>Terdapat kesalahan: </h5>
                    <div class="text-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection