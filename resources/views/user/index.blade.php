@extends('layouts.template')
@section('title', 'Data Kasir')
@push('style')
<!-- SweetAlert -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/css/sweetalert2.min.css')}}">
@endpush

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Data Kasir</h4>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <a href="{{ route('user.create') }}" class="btn btn-primary btn-icon-split mr-1"
                            data-toggle="tooltip" data-placement="top" title="Tambah Data Kasir">
                            <span class="icon text-white-50">
                                <i class="fas fa-plus"></i> 
                            </span>
                            <span class="text">Tambah Data</span>
                        </a>
                        <span data-toggle="modal" data-target="#modalFilter">
                            <a class="btn btn-info btn-icon-split mr-1" 
                                data-toggle="tooltip" data-placement="top" title="Filter Data Kasir">
                                <span class="icon text-white-50">
                                    <i class="fas fa-filter"></i> 
                                </span>
                                <span class="text">Filter</span>
                            </a>
                        </span>
                        @if (request('role') || request('username') || request('email'))
                            <a href="{{ route('user.index') }}" class="btn btn-danger btn-icon-split mr-1"
                                data-toggle="tooltip" data-placement="top" title="Bersihkan Filter">
                                <span class="icon text-white-50">
                                    <i class="fas fa-broom"></i> 
                                </span>
                                <span class="text">Bersihkan Filter</span>
                            </a>
                        @endif
                    </div>
                </div>
                @if (request('role') || request('username') || request('email'))
                    <p class="text-secondary">
                        Filter saat ini : <br>
                        {!! request('role') ? '&emsp;&emsp; Role : ' . request('role') . '<br>' : '' !!}
                        {!! request('email') ? '&emsp;&emsp; Email : ' . request('email') . '<br>' : '' !!}
                        {!! request('username') ? '&emsp;&emsp; Username : ' . request('username') . '<br>' : '' !!}
                    </p>
                @endif
                @if ($data->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="table-user">
                            <thead>
                                <tr role="row" style="background-color:#dadada">
                                    <th class="text-center" style="width: 5%;">No</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th class="text-center" style="width: 15%;"><i class="fas fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                <tr>
                                    <td>{{ (($data->currentPage() - 1) * $data->perPage()) + $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->role->name }}</td>
                                    <td class="text-center">
                                        <form action="{{ route('user.respass', [$item->id]) }}" method="POST"
                                            id="form-respass-{{$item->id}}" style="display: inline">
                                            @csrf
                                            @method('PUT')
                                            <button type="submit" class="btn btn-info btn-sm"
                                                onclick="submit_respass({{$item->id}})" data-container="table"
                                                data-toggle="tooltip" data-placement="top" title="Reset Password Kasir">
                                                <i class="fas fa-key"></i>
                                            </button>
                                        </form>
                                        <a href="{{ route('user.edit', [$item->id]) }}"
                                            class="btn btn-sm btn-warning btn-action mx-1" data-container="table"
                                            data-toggle="tooltip" data-placement="top" title="Edit Data Kasir">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <form action="{{ route('user.destroy', [$item->id]) }}" method="POST"
                                            id="form-delete-{{$item->id}}" style="display: inline">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm"
                                                onclick="submit_delete({{$item->id}})" data-container="table"
                                                data-toggle="tooltip" data-placement="top" title="Hapus Data Kasir">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="d-flex justify-content-center mt-3">
                        {!! $data->links() !!}
                    </div>
                @else
                    <div class="text-center">
                        <h3>Tidak ada data ditemukan.</h3>
                        <img src="{{ asset('img/data_empty.jpg') }}" width="427px" height="240px">
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalFilter" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Filter Data</h5>
                    <button class="close" type="button" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('user.index') }}" class="form-horizontal" id="form-filter">
                        <div class="text-secondary small mb-2">Filter dapat diisi semua atau hanya salah satunya.</div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Role :</label>
                            <div class="col-lg-8">
                                <div class="form-check">
                                    <input class="form-check-input" name="role" id="role-admin" type="checkbox" value="admin">
                                    <label class="form-check-label" for="role-admin">Admin</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" name="role" id="role-kasir" type="checkbox" value="kasir">
                                    <label class="form-check-label" for="role-kasir">Kasir</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Username :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="username" maxlength="50" placeholder="Username" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Email :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="email" maxlength="50" placeholder="Email" autocomplete="off">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <input class="btn btn-success" type="submit" value="Filter" form="form-filter">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<!-- Script Tambahan -->
    <script src="{{ asset('plugins/sweetalert2/js/sweetalert2.all.min.js') }}"></script>
    <script type="text/javascript">
        $('[data-toggle="tooltip"]').tooltip();
        $('#form-filter').on('submit', function () {
            $('input[name=username]').val() == '' ? $('input[name=username]').removeAttr('name') : false;
            $('input[name=email]').val() == '' ? $('input[name=email]').removeAttr('name') : false;
        });
        function submit_delete(user_id) {
            event.preventDefault();
            Swal.fire({
                width: 680,
                title: 'Anda yakin ingin menghapus data terpilih?',
                text: "Data yang sudah di hapus tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value == true) {
                    $(`#form-delete-${user_id}`).submit();
                } else {
                    return false;
                }
            });
        }

        function submit_respass(user_id) {
            event.preventDefault();
            Swal.fire({
                width: 680,
                title: 'Konfirmasi reset password kasir terpilih!',
                text: "Beritahu kasir untuk menggantinya segera di menu profil!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Iya',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value == true) {
                    $(`#form-respass-${user_id}`).submit();
                } else {
                    return false;
                }
            });
        }
    </script>
@endpush
