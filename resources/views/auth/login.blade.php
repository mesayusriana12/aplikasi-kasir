<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kasirku | Login</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('plugins/fontawesome/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
	<style>
		.login-logo{
			font-size: 2.1rem;
			font-weight: 300;
			margin-bottom: .9rem;
			text-align: center;
			color: gray;
		}
	</style>
</head>
<body class="bg-gradient-primary">
	@include('sweetalert::alert')
	<div class="container">
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-md-6 col-sm-6">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
									<div class="login-logo">
										<img src="{{ asset('img/logo.png') }}" alt="Logo Kasirku" width="64px">
									</div>
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Portal Login Kasirku</h1>
                                    </div>
                                    <form action="{{ route('authenticate') }}" method="post">
										@csrf
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user" name="username"
                                                placeholder="Masukan Username atau Email..." required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" name="password"
                                                placeholder="Masukan Password ..." required>
                                        </div>
                                        <input class="btn btn-primary btn-user btn-block" value="Login" type="submit">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('plugins/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
</body>
</html>
